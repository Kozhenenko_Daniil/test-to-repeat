package task3;

import java.util.Scanner;

/**
 * Класс для перевода рублей в евро по заданному курсу
 *
 * @author Kozhenenko D.D
 */
public class Task3 {
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.println("Введите сумму в рублях,которую хотите перевести  -  ");
        double ruble = scanner.nextDouble();
        System.out.println("Введите курс - ");
        double course = scanner.nextDouble();
        System.out.println ("Ваша сумма в евро - " + String.format ("%.2f", ruble / course));
    }
}

