package task7;

import java.util.Scanner;

/**
 * Класс, который проверяет является ли число типа double целым
 *
 * @author Kozhenenko D.D
 */
public class Task7 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите число типа double - ");
        double a = scanner.nextDouble();

        isInteger(a);

    }

    private static void isInteger(double a) {
        if (a % 1 == 0) System.out.println("Заданное число целое");
        else System.out.println("Заданное число не является целым");
    }
}
