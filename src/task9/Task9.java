package task9;

import java.util.Scanner;
/**
 * Класс, который выводит является ли заданное число палиндромом или нет
 *
 * @author Kozhenenko D.D
 */
public class Task9 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите число - ");
        int a = scanner.nextInt();
        String i = String.valueOf(a);

        isPalindrome(i);
    }

    private static void isPalindrome(String i) {
        boolean a = false;
        if (i.equals(new StringBuilder().append(i).reverse().toString())) {
            a = true;
        }
        if (a) System.out.println("Это палиндром");
        else System.out.println("Это не палиндром");

    }
}
