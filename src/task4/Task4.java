package task4;
/**
 * Класс для расчета расстояния до точки удара молнии
 *
 * @author Kozhenenko D.D
 */
public class Task4 {
    public static void main(String[] args) {
        double speed = 1234.8;
        double interval = 6.8;
        double distance = speed*interval;
        System.out.println("Расстояние составляет " + String.format("%.2f", distance) + " км");

    }
}
