package task5;

public class Task5 {
    public static void main(String[] args) {

        for (int i = 2; i < 100; i++) {
            int x = 0;
            for (int j = 1; j <= i; j++) {
                if (i % j == 0) {
                    x++;
                }
            }
            if (x <= 2) {
                System.out.println(i + " - простое число ");
            }
        }
    }
}