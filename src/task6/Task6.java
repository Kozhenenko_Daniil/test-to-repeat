package task6;

import java.util.Scanner;

/**
 * Класс вывода таблицы умножения введенного пользователем числа с клавиатуры
 *
 * @author Kozhenenko D.D
 */
public class Task6 {
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.println("Введите ваше число - ");
        int number = scanner.nextInt();

        for (int i = 1; i <= 10; i++) {
            System.out.println(number + " * " + i + " = " + number * i);

        }
    }
}
