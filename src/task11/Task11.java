package task11;

import java.util.Scanner;

/**
 * Класс рассчета часов, минут и секунд в n-ном количестве суток
 *
 * @author Kozheneko D.D
 */
public class Task11 {
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.println("Введите кол-во суток - ");
        int numberOfDay = scanner.nextInt();
        System.out.println("Количество часов = " + numberOfDay * 24);
        System.out.println("Количество минут = " + numberOfDay * 24 * 60);
        System.out.println("Количество секунд = " + numberOfDay * 24 * 3600);

    }
}