package task1;

import java.util.Scanner;

/**
 * Класс, который считывает символы пока не встретится точка и выводит количество пробелов
 *
 * @author Kozhenenko D.D
 */

public class Task1 {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in).useDelimiter("\\.");
        System.out.println("Введите текст: ");
        String n = scanner.next();
        System.out.println(n);
        int i = n.indexOf(" ");
        System.out.println("Количество пробелов:" + i);



    }
}
