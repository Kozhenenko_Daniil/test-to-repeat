package task13;

import java.util.Scanner;
/**
 * Класс, определяющий является ли символ введенный с клавиатуры цифрой, буквой или знаком пунктуации
 *
 * @author Kozhenenko D.D
 */
public class Task13 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите символ - ");
        String symbol = scanner.next();
        char a = symbol.charAt(0);
        if (Character.isDigit(a)) System.out.println("Символ является цифрой");
        if (Character.isLetter(a)) System.out.println("Символ является буквой");
        if (".,:;!?".contains(symbol)) System.out.println("Символ является знаком припенания");
    }

}
