package task2;
import java.util.Scanner;
import java.util.Arrays;

/**
 * Класс увеличивающий заданный элемент массива на 10%
 *
 * @author Kozhenenko D.D
 */
public class Task2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner (System.in);
        double[] box = {16, 195, 2000, 100, 96, 12345};
        System.out.println (Arrays.toString (box));
        int number;
        System.out.println("Введите номер числа от 0 до 5 в массиве, которое хотите увеличить на 10%: ");
        number = scanner.nextInt();
        double[] increasedBox = increasedBox (box, number);
        System.out.println(Arrays.toString(increasedBox));
    }

    /**
     * @param box исходный массив
     * @param number выбранный пользователем элемент массива
     *
     * @return конечный массив
     */
    private static double[] increasedBox(double[] box, int number) {
       box[number] = box[number]+ box[number] * 0.1;
        return box;
    }
}
